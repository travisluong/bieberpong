require 'spec_helper'

describe "highscores/show" do
  before(:each) do
    @highscore = assign(:highscore, stub_model(Highscore,
      :name => "Name",
      :country => "Country",
      :score => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Country/)
    rendered.should match(/1/)
  end
end
