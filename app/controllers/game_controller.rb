class GameController < ApplicationController
  def create_highscore
    @highscore = Highscore.new(highscore_params)
    respond_to do |format|
      if @highscore.save
        @highscores = Highscore.top_ten
        @count = Highscore.count
        @data = {
          highscores: @highscores,
          count: @count
        }
        format.json { render json: @data }
      else
        format.json { render json: @highscore.errors }
      end
    end
  end

  def get_highscores
    @highscores = Highscore.top_ten
    respond_to do |format|
      format.json { render json: @highscores }
    end
  end

  private

    # Only allow a trusted parameter "white list" through.
    def highscore_params
      params.require(:highscore).permit(:name, :country, :score)
    end
end
