class HighscoresController < ApplicationController
  before_action :set_highscore, only: [:show, :edit, :update, :destroy]

  # GET /highscores
  def index
    @highscores = Highscore.all
  end

  # GET /highscores/1
  def show
  end

  # GET /highscores/new
  def new
    @highscore = Highscore.new
  end

  # GET /highscores/1/edit
  def edit
  end

  # POST /highscores
  def create
    @highscore = Highscore.new(highscore_params)

    if @highscore.save
      redirect_to @highscore, notice: 'Highscore was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /highscores/1
  def update
    if @highscore.update(highscore_params)
      redirect_to @highscore, notice: 'Highscore was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /highscores/1
  def destroy
    @highscore.destroy
    redirect_to highscores_url, notice: 'Highscore was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_highscore
      @highscore = Highscore.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def highscore_params
      params.require(:highscore).permit(:name, :country, :score)
    end
end
