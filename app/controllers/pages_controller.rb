class PagesController < ApplicationController
  def home
    @highscores = Highscore.top_ten
  end

  def scores
    if params[:page]
      @rank_offset = (params[:page].to_i - 1) * 10
    else
      @rank_offset = 0
    end
    @highscores = Highscore.order('score DESC').order('created_at ASC').paginate(:page => params[:page], :per_page => 10)
  end
end
