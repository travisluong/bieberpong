class Highscore < ActiveRecord::Base
  scope :top_ten, -> { limit(10).order('score DESC').order('created_at ASC') }

  validates :name, :country, :score, presence: true
  validates :country, inclusion: { in: %w{USA Canada} }
  validates :name, length: { minimum: 1, maximum: 11 }
end
